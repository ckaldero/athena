#ifndef TRIGT1CAL_CPMRoICOLLECTION_H
#define TRIGT1CAL_CPMRoICOLLECTION_H

#include "AthContainers/DataVector.h"
#include "TrigT1CaloEvent/CPMRoI.h"

/** Container class for CPMRoI objects */

using namespace LVL1;
typedef DataVector<CPMRoI> CPMRoICollection;

#endif
