/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

/**
@page TrkSegmentConverter_page TrkSegmentConverter Package

>> Please enter an overview of the created package here: What does it contain?
This package implements the SegmentConverter interface. It uses a TrackFitter to convert the Segments into full Tracks.

@author Christian Schmitt <Christian.Schmitt@cern.ch>




*/
